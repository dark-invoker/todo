/* PROJECT MODAL */
var btn = document.querySelector('#call-modal'),
    modal = document.querySelector('.modal'),
    closebtn = document.querySelector('.close-btn');

btn.addEventListener('click', function () {
    $('.modal .modal-wrapper .modal-header h3').text('Adding project');
    clearProjFields();
    modal.style.display = 'flex';
});
closebtn.addEventListener('click', function () {
    modal.style.display = "none";
});
window.addEventListener('click', function (e) {
    if (e.target == modal) {
        modal.style.display = 'none';
    }
});

/* TASK MODAL */

$(document).on('click', '.icon-edit.task', function () {
    clearTaskFields();
    $('.task-modal').css('display', 'flex');
});
$(document).on('click', '.close-task-btn', function () {
    $('.task-modal').css('display', 'none');
});
var modalTask = document.querySelector('.task-modal');
window.addEventListener('click', function (e) {
    if (e.target == modalTask) {
        modalTask.style.display = 'none';
    }
});

//ADD Proj
$(document).on('click', '#submit-proj', function () {
    $.ajax({
        url: "projects",
        type: "POST",
        data: ({
            name: $("#proj-name").val(),
            _token: $('input[name=_token]').val()
        }),
        dataType: "html",
        success: function (data) {
            if (data != false) {
                data = $.parseJSON(data);
                $(drawProjBlock(data)).insertAfter(".modal");
                $('.modal').css('display', 'none');
            }
        }

    });
});

// Delete Proj
$(document).on('click', '.icon-delete.project', function () {
    $.ajax({
        url: "projects/" + $(this).attr('value'),
        type: "DELETE",
        data: ({_token: $('input[name=_token]').val()}),
        dataType: "html",
        success: function (data) {
            data = '.card.' + $.parseJSON(data);
            $(data).remove();
        }
    });
});

//Edit Proj
$(document).on('click', '.icon-edit.project', function () {
    $('#submit-proj').css('display', 'none');
    $('#submit-edit-proj').css('display', 'inline');
    $('.modal').css('display', 'flex');
    $('#submit-edit-proj').attr('project-id', $(this).attr('value'));
    $('.modal .modal-wrapper .modal-header h3').text('Edit project ' +
        $('.card.' + $(this).attr('value') + ' .card-header .d-inline').text());
    $('#proj-name').val($('.card.' + $(this).attr('value') + ' .card-header .d-inline').text());
});

//Save Proj
$(document).on('click', '#submit-edit-proj', function () {
    $('.modal').css('display', 'none');
    $('#submit-proj').css('display', 'inline');
    $('#submit-edit-proj').css('display', 'none');

    $.ajax({
        url: "projects/" + $('#submit-edit-proj').attr('project-id'),
        type: "PUT",
        data: ({
            name: $("#proj-name").val(),
            _token: $('input[name=_token]').val()
        }),
        dataType: "html",
        success: function (data) {
            let params = $.parseJSON(data);
            let tmpClass = '.card.' + params.id;
            $(tmpClass + ' .card-header .d-inline').text(params.name);
            clearProjFields();
        }
    });
});

function clearProjFields() {
    $("#proj-name").val('');
    $("#proj-deadline").val('');
}

function clearTaskFields() {
    $("#task-name").val('');
    $("#task-deadline").val('');
}

function drawProjBlock(data) {
    let str = '<div class="card ' + data.id + '">' +
        '<div class="card-header">' +
        '<div class="d-inline">' + data.name + '</div>' +
        '<div class="float-right"><span class="icon-delete project ml-2" value="' + data.id + '"></span></div>' +
        '<div class="float-right">' + '<span class="icon-edit project" value="' + data.id + '"> |</span></div></div>' +
        '<div class="m-2 text-center form-group row justify-content-center">' +
        '<input type="text" project-id="' + data.id + '" class="task-name col-10 form-control mr-2" placeholder="enter task name">' +
        '<input type="button" project-id="' + data.id + '" value="add" class="task-submit col btn btn-primary" placeholder="enter task name">' +
        '</div> <div class="card-body ' + data.id + ' p-0"></div><br></div>';
    return str;
}

//Datepicker
$(document).on("focus", "#proj-deadline", function () {
    $(this).datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: (new Date()).toISOString().slice(0, 10)
    });
});

$(document).on("focus", "#task-deadline", function () {
    $(this).datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: (new Date()).toISOString().slice(0, 10)
    });
});

//ADD Task
$(document).on('click', '.task-submit', function () {
    let projId = $(this).attr('project-id');
    let obj = $('.task-name[project-id = ' + projId + ']');
    $.ajax({
        url: "tasks",
        type: "POST",
        data: ({
            project_id: projId,
            name: obj.val(),
            _token: $('input[name=_token]').val()
        }),
        dataType: "html",
        success: function (data) {
            if (data != false) {
                data = $.parseJSON(data);
                $('.card-body.' + data.project_id).prepend(drawTaskBlock(data));
            }
        }
    });
});

function drawTaskBlock(obj) {
    let str = '';
    if (obj.deadline == (new Date()).toISOString().slice(0, 10)) {
        str = '<div priority="' + obj.priority + '" class="task-row p-2 alert-danger" task-id="'+ obj.id +'" priority="' + obj.priority + '">' +
            '<span class="pl-2"><input type="checkbox" task-id="' + obj.id + '"></span>' +
            '<span class="ml-3 title-task-name' + obj.id + '">' + obj.name +
            '</span> <span class="float-right"><span class="date" style="margin-right: .75rem!important;">' + obj.deadline + '</span>' +
            '<span task-id="' + obj.id + '" project-id="' + obj.project_id + '" class="icon-up task" priority="' + obj.priority + '"> | </span>' +
            '<span task-id="' + obj.id + '" project-id="' + obj.project_id + '" class="icon-down task" priority="' + obj.priority + '"> | </span> ' +
            '<span task-id="' + obj.id + '" project-id="' + obj.project_id + '" class="icon-edit task"> | </span> <span task-id="' + obj.id + '" class="icon-delete task mr-2">' +
            '</span></span></div>';
    } else {
        str = '<div priority="' + obj.priority + '" class="task-row p-2" task-id="'+obj.id+'" priority="' + obj.priority + '">' +
            '<span class="pl-2"><input type="checkbox" task-id="' + obj.id + '"></span>' +
            '<span class="ml-3 title-task-name' + obj.id + '">' + obj.name +
            '</span> <span class="float-right"><span class="date" style="margin-right: .75rem!important;">' + obj.deadline + '</span>' +
            '<span task-id="' + obj.id + '" project-id="' + obj.project_id + '" class="icon-up task" priority="' + obj.priority + '"> | </span>' +
            '<span task-id="' + obj.id + '" project-id="' + obj.project_id + '" class="icon-down task" priority="' + obj.priority + '"> | </span> ' +
            '<span task-id="' + obj.id + '" project-id="' + obj.project_id + '" class="icon-edit task"> | </span> <span task-id="' + obj.id + '" class="icon-delete task mr-2">' +
            '</span></span></div>';
    }

    return str;
}

//Edit task
$(document).on('click', '.icon-edit.task', function () {
    $('#submit-edit-task').attr('task-id', $(this).attr('task-id'));
    $('#submit-edit-task').attr('project-id', $(this).attr('project-id'));
    $('#task-name').val($('.title-task-name' + $(this).attr('task-id')).text());
    let parent = $(this).parent().parent();
    $('#submit-edit-task').attr('priority', parent.attr('priority'));
});

//Save task
$(document).on('click', '#submit-edit-task', function () {
    $('.task-modal').css('display', 'none');
    $.ajax({
        url: "tasks/" + $('#submit-edit-task').attr('task-id'),
        type: "PUT",
        data: ({
            name: $("#task-name").val(),
            priority: $("#submit-edit-task").attr('priority'),
            project_id: $('#submit-edit-task').attr('project-id'),
            deadline: $("#task-deadline").val(),
            _token: $('input[name=_token]').val()
        }),
        dataType: "html",
        success: function (data) {
            let params = $.parseJSON(data);
            if (params.deadline != (new Date()).toISOString().slice(0, 10)) {
                let parent = $('.title-task-name' + params.id).parent();
                parent.removeClass('alert-danger');

                console.log($('.card-body.' + params.project_id + ' .task-row[priority=' + params.priority
                    + '] .float-right .date'));
                $('.card-body.' + params.project_id + ' .task-row[priority=' + params.priority
                    + '] .float-right .date').text(params.deadline);
            }
            $('.title-task-name' + params.id).text(params.name);
            clearTaskFields();
        }
    });
});

//Delete Task
$(document).on('click', '.icon-delete.task', function () {
    $.ajax({
        url: "tasks/" + $(this).attr('task-id'),
        type: "DELETE",
        data: ({_token: $('input[name=_token]').val()}),
        dataType: "html",
        success: function (data) {
            data = $.parseJSON(data);
            $('.title-task-name' + data.id).parent().remove();

            let length = $('.card-body.' + data.project_id + ' .task-row').length;
            $('.card-body.' + data.project_id + ' .task-row').each(function (index, value) {
                $(value).attr('priority', length - index);
            });
        }
    });
});

//Complete Task
$(document).on('click', 'input[type=checkbox]', function () {
    $.ajax({
        url: "tasks/" + $(this).attr('task-id') + "/complete/",
        type: "PUT",
        data: ({_token: $('input[name=_token]').val()}),
        dataType: "html",
        success: function (data) {
            data = $.parseJSON(data);
            let id = data.id;
            let name = $('.title-task-name' + id).text();
            $('.title-task-name' + id).parent().remove();
            $('.card-body.' + data.project_id).append(
                '<div class="task-row-tmp p-2 alert-success" priority="' + 0 + '">' +
                '<span class="complete ml-3 title-task-name' + id + '">' +
                name + '</span></div>');

            let length = $('.card-body.' + data.project_id + ' .task-row').length;
            $('.card-body.' + data.project_id + ' .task-row').each(function (index, value) {
                $(value).attr('priority', length - index);
            });
        }
    });
});

//Increase Task
$(document).on('click', '.icon-up.task', function () {
    let nextTaskId = $('.card-body.' + $(this).attr('project-id') + ' .task-row[priority=' + (+$(this).attr('priority') + 1) + ']').attr('task-id');
    let nextPriority = $('.card-body.' + $(this).attr('project-id') + ' .task-row[priority=' + (+$(this).attr('priority') + 1) + ']').attr('priority');

    if (nextPriority != undefined) {
        $.ajax({
            url: "tasks/" + $(this).attr('task-id') + "/swapPriority",
            type: "PUT",
            data: ({
                next_task_id: nextTaskId,
                next_priority: nextPriority,
                current_task_id: $(this).attr('task-id'),
                current_priority: $(this).attr('priority'),
                project_id: $(this).attr('project-id'),
                _token: $('input[name=_token]').val()
            }),
            dataType: "html",
            success:
                function (param) {
                    param = $.parseJSON(param);
                    if (param.isOk == true) {
                        let firstTaskRow = $('.card-body.' + param.project_id + ' .task-row[priority=' + param.current_priority + ']');
                        let secondTaskRow = $('.card-body.' + param.project_id + ' .task-row[priority=' + param.next_priority + ']');

                        $(firstTaskRow).insertBefore(secondTaskRow);

                        firstTaskRow.attr('priority', param.next_priority);
                        secondTaskRow.attr('priority', param.current_priority);

                        $('.card-body.' + param.project_id + ' .task-row[priority=' + param.next_priority
                            + '] .float-right .icon-up').attr('priority', param.next_priority);
                        $('.card-body.' + param.project_id + ' .task-row[priority=' + param.next_priority
                            + '] .float-right .icon-down').attr('priority', param.next_priority);
                        $('.card-body.' + param.project_id + ' .task-row[priority=' + param.current_priority
                            + '] .float-right .icon-up').attr('priority', param.current_priority);
                        $('.card-body.' + param.project_id + ' .task-row[priority=' + param.current_priority
                            + '] .float-right .icon-down').attr('priority', param.current_priority);
                    }
                }
        });
    }
});

//Reduce Task
$(document).on('click', '.icon-down.task', function () {
    let previousTaskId = $('.card-body.' + $(this).attr('project-id') + ' .task-row[priority=' + (+$(this).attr('priority') - 1) + ']').attr('task-id');
    let previousPriority = $('.card-body.' + $(this).attr('project-id') + ' .task-row[priority=' + (+$(this).attr('priority') - 1) + ']').attr('priority');

    if (previousPriority != undefined) {
        $.ajax({
            url: "tasks/" + $(this).attr('task-id') + "/swapPriority",
            type: "PUT",
            data: ({
                next_task_id: previousTaskId,
                next_priority: previousPriority,
                current_task_id: $(this).attr('task-id'),
                current_priority: $(this).attr('priority'),
                project_id: $(this).attr('project-id'),
                _token: $('input[name=_token]').val()
            }),
            dataType: "html",
            success:
                function (param) {
                    param = $.parseJSON(param);
                    if (param.isOk == true) {
                        let nextPriority = +param.current_priority - 1;
                        let firstTaskRow = $('.card-body.' + param.project_id + ' .task-row[priority=' + param.current_priority + ']');
                        let secondTaskRow = $('.card-body.' + param.project_id + ' .task-row[priority=' + param.next_priority + ']');

                        $(secondTaskRow).insertBefore(firstTaskRow);
                        firstTaskRow.attr('priority', param.next_priority);
                        secondTaskRow.attr('priority', param.current_priority);

                        $('.card-body.' + param.project_id + ' .task-row[priority=' + param.next_priority
                            + '] .float-right .icon-down').attr('priority', param.next_priority);
                        $('.card-body.' + param.project_id + ' .task-row[priority=' + param.next_priority
                            + '] .float-right .icon-up').attr('priority', param.next_priority);
                        $('.card-body.' + param.project_id + ' .task-row[priority=' + param.current_priority
                            + '] .float-right .icon-down').attr('priority', param.current_priority);
                        $('.card-body.' + param.project_id + ' .task-row[priority=' + param.current_priority
                            + '] .float-right .icon-up').attr('priority', param.current_priority);
                    }
                }
        });
    }
});


var url = window.location.origin + '/';
