<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['web', 'auth'])->group(function () {
    Route::get('/home', 'HomeController@index');

    Route::prefix('projects')->group(function () {
        Route::post('/', 'ProjectController@create');

        Route::put('{id}', 'ProjectController@edit');

        Route::delete('{id}', 'ProjectController@delete');
    });

    Route::prefix('tasks')->group(function () {
        Route::post('/', 'TaskController@create');

        Route::put('{id}', 'TaskController@edit');

        Route::delete('{id}', 'TaskController@delete');

        Route::put('{id}/complete', 'TaskController@complete');

        Route::put('{id}/swapPriority', 'TaskController@swapPriority');
    });
});



