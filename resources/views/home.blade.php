@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <!-- BUTTON ADD -->
                <div class="text-center">
                    <button id="call-modal" class="mb-4 btn-lg btn-primary btn">ADD PROJECT</button>
                </div>

                <!-- MODAL -->
                <div class="modal">
                    <div class="modal-wrapper">

                        <div class="modal-header">
                            <h3>Adding project</h3>
                            <button class="close-btn btn">×</button>
                        </div>

                        <div class="modal-body">
                            <div class="form-group row">
                                <span class="col-md-3 col-form-label text-center">Project name: </span>
                                <input type="text" maxlength="100" size="40" name="proj-name" id="proj-name"
                                       class="col-md-7 form-control" required autofocus>
                            </div>
                            <div class="text-center">
                                <input type="button" value="Add" id="submit-proj" class="btn btn-primary col-md-2">
                                <input type="button" value="Save" id="submit-edit-proj"
                                       class="btn btn-primary col-md-2">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END MODAL -->
                <!-- TASK MODAL -->
                <div class="task-modal">
                    <div class="modal-wrapper">

                        <div class="modal-header">
                            <h3>Edit task</h3>
                            <button class="close-task-btn btn">×</button>
                        </div>

                        <div class="modal-body">
                            <div class="form-group row">
                                <span class="col-md-3 col-form-label text-center">Task name: </span>
                                <input type="text" maxlength="100" size="40" id="task-name"
                                       class="col-md-7 form-control" required autofocus>
                            </div>
                            <div class="form-group row">
                                <span class="col-md-3 col-form-label text-right">Deadline: </span>
                                <input type="text" id="task-deadline" class="col-md-7 form-control"
                                       required>
                            </div>
                            <div class="text-center">
                                <input type="button" value="Save" id="submit-edit-task"
                                       class="btn btn-primary col-md-2">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END TASK MODAL -->

                @if(!empty($manager))
                    @foreach($manager as $item)
                        <div class="card {{ $item['project']['id'] }}">
                            <div class="card-header">
                                <div class="d-inline">{{ $item['project']['name'] }}</div>
                                <div class="float-right">
                                    <span class="icon-delete project ml-2" value="{{ $item['project']['id'] }}"></span>
                                </div>
                                <div class="float-right">
                                    <span class="icon-edit project" value="{{ $item['project']['id'] }}"> |</span>
                                </div>
                            </div>

                            <div class="m-2 text-center form-group row justify-content-center">
                                <input project-id="{{ $item['project']['id'] }}" type="text"
                                       class="task-name col-10 form-control mr-2" placeholder="enter task name">
                                <input project-id="{{ $item['project']['id'] }}" type="button"
                                       class="task-submit col-md btn btn-primary" value="add">
                            </div>
                            <div class="card-body {{ $item['project']['id'] }} p-0">
                                @if(isset($item['tasks']))
                                    @foreach($item['tasks'] as $task)
                                        @if($task['status'] == 1)
                                            <div class="task-row-tmp p-2 alert-success"
                                                 priority="{{ $task['priority'] }}" task-id="{{ $task['id'] }}">
                                                <span
                                                    class="complete ml-3 title-task-name{{ $task['id'] }}">{{ $task['name'] }}</span>
                                            </div>
                                        @else
                                            @if($task['deadline'] == date('Y-m-d'))
                                                <div class="task-row p-2 alert-danger"
                                                     priority="{{ $task['priority'] }}" task-id="{{ $task['id'] }}">
                                                    <span class="pl-2"><input task-id="{{ $task['id'] }}"
                                                                              type="checkbox"></span>
                                                    <span
                                                        class="ml-3 title-task-name{{ $task['id'] }}">{{ $task['name'] }}</span>

                                                    <span class="float-right">
                                            <span class="date mr-2">{{$task['deadline'] }}</span>
                                            <span task-id="{{ $task['id'] }}" project-id="{{ $item['project']['id'] }}"
                                                  class="icon-up task" priority="{{ $task['priority'] }}"> | </span>
                                            <span task-id="{{ $task['id'] }}" project-id="{{ $item['project']['id'] }}"
                                                  class="icon-down task" priority="{{ $task['priority'] }}"> | </span>
                                            <span task-id="{{ $task['id'] }}" project-id="{{ $item['project']['id'] }}"
                                                  class="icon-edit task"> | </span>
                                            <span task-id="{{ $task['id'] }}" class="icon-delete task mr-2"></span>
                                            </span>
                                                </div>
                                            @else
                                                <div class="task-row p-2" priority="{{ $task['priority'] }}" task-id="{{ $task['id'] }}">
                                                    <span class="pl-2"><input task-id="{{ $task['id'] }}"
                                                                              type="checkbox"></span>
                                                    <span
                                                        class="ml-3 title-task-name{{ $task['id'] }}">{{ $task['name'] }}</span>
                                                    <span class="float-right">
                                                <span class="date mr-2">{{ $task['deadline'] }}</span>
                                                <span task-id="{{ $task['id'] }}"
                                                      project-id="{{ $item['project']['id'] }}" class="icon-up task"
                                                      priority="{{ $task['priority'] }}"> | </span>
                                                <span task-id="{{ $task['id'] }}"
                                                      project-id="{{ $item['project']['id'] }}" class="icon-down task"
                                                      priority="{{ $task['priority'] }}"> | </span>
                                                <span task-id="{{ $task['id'] }}"
                                                      project-id="{{ $item['project']['id'] }}" class="icon-edit task"> | </span>
                                                <span task-id="{{ $task['id'] }}" class="icon-delete task mr-2"></span>
                                        </span>
                                                </div>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
