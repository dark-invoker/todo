<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('t_tasks')->insert([
            [
                'name' => 'first task',
                'deadline' => date('Y-m-d') . ' 23:59:59',
                'status' => 0,
                'priority' => 1,
                'project_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'second task',
                'deadline' => date('Y-m-d') . ' 23:59:59',
                'status' => 0,
                'priority' => 2,
                'project_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'third task',
                'deadline' => date('Y-m-d H:i:s'),
                'status' => 0,
                'priority' => 3,
                'project_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'fourth task',
                'deadline' => date('Y-m-d H:i:s'),
                'status' => 0,
                'priority' => 4,
                'project_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
}
