<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->tinyInteger('status');
            $table->smallInteger('priority');
            $table->date('deadline');
            $table->unsignedBigInteger('project_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_tasks');
    }
}
