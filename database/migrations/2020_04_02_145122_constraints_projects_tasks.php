<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConstraintsProjectsTasks extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('t_projects', function (Blueprint $table) {
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::table('t_tasks', function (Blueprint $table) {
            $table
                ->foreign('project_id')
                ->references('id')
                ->on('t_projects')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('t_projects', function (Blueprint $table) {
            $table->dropForeign('t_projects_user_id_foreign');
        });
        Schema::table('t_projects', function (Blueprint $table) {
            $table->dropForeign('t_tasks_project_id_foreign');
        });
    }
}
