<?php


namespace App\Services\Project;


use App\Models\Project;
use App\Models\Task;

class ProjectService
{
    public function getProjectsWithTasksByUserId(int $userId)
    {
        $columns = [
            Project::TABLE . '.id AS project_id',
            Project::TABLE . '.name AS project_name',
            Task::TABLE . '.id AS task_id',
            Task::TABLE . '.name AS task_name',
            Task::TABLE . '.deadline AS task_deadline',
            Task::TABLE . '.status AS task_status',
            Task::TABLE . '.priority AS task_priority',
        ];

        return Project::select($columns)
            ->withTasks()
            ->byUserId($userId)
            ->where(Task::TABLE . '.deleted_at', null)
            ->orderBy(Project::TABLE.'.id', 'desc')
            ->orderBy(Task::TABLE.'.priority', 'desc');
    }
}
