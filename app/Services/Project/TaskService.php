<?php


namespace App\Services\Project;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Support\Facades\DB;

class TaskService
{
    public function maxPriorityInProject(int $projectId): int
    {
        $taskTable = Task::TABLE;
        $columns = [
            DB::raw("max({$taskTable}.priority) AS max_priority")
        ];

        return Project::select($columns)
                ->withTasks()
                ->where(Task::TABLE . '.deleted_at', null)
                ->get()[0]->max_priority ?? 0;
    }

    public function swapPriority(int $taskId, int $priority, int $nextTaskId, int $nextPriority): bool
    {
        $fOk = true;
        try {
            Task::whereId($taskId)->update(['priority' => $nextPriority]);
            Task::whereId($nextTaskId)->update(['priority' => $priority]);
        } catch (\Throwable $t) {
            $fOk = false;
        }
        return $fOk;
    }

    public function previousPriority(int $taskId): int
    {
        return 0;
    }
}
