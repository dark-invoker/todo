<?php


namespace App\Structure\Project;


class ProjectStructure
{
    public static function projectsWithTasks(array &$projectsWithTasks)
    {
        $structured = [];
        foreach ($projectsWithTasks as $key => &$val) {
            if (!is_null($val['task_id'])) {
                $structured[$val['project_id']]['tasks'][] = [
                    'id' => $val['task_id'],
                    'name' => $val['task_name'],
                    'status' => $val['task_status'],
                    'priority' => $val['task_priority'],
                    'deadline' => $val['task_deadline'],
                ];
            }

            $project = [
                'id' => $val['project_id'],
                'name' => $val['project_name'],
            ];

            if (empty($structured[$val['project_id']]['project'])) {
                $structured[$val['project_id']]['project'] = $project;
            }
        }

        return $structured;
    }
}
