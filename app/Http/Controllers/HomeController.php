<?php

namespace App\Http\Controllers;

use App\Structure\Project\ProjectStructure;
use App\Services\Project\ProjectService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    protected $projectService;

    public function __construct()
    {
        $this->projectService = new ProjectService();
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        $service = &$this->projectService;

        $projsWithTasks = $service->getProjectsWithTasksByUserId($user->id)->get()->toArray();
        $structured = ProjectStructure::projectsWithTasks($projsWithTasks);

        return view('home', ['manager' => $structured]);
    }
}
