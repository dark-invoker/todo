<?php

namespace App\Http\Controllers;

use App\Services\Project\TaskService;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    protected $taskService;

    public function __construct()
    {
        $this->taskService = new TaskService();
    }

    public function create(Request $request)
    {
        $service = &$this->taskService;

        try {
            $maxPriority = $service->maxPriorityInProject($request->project_id);

            $newTask = new Task();
            $newTask->name = $request->name;
            $newTask->deadline = date('Y-m-d');
            $newTask->status = 0;
            $newTask->priority = ++$maxPriority;
            $newTask->project_id = $request->project_id;
            $newTask->save();
        } catch (\Throwable $t) {
            throw new \Exception('Can\'t add task.');
        }
        return response()->json($newTask);
    }

    public function edit(Request $request)
    {
        $taskId = $request->id;
        $name = $request->name;
        $deadline = $request->deadline;

        if (empty($deadline)) {
            $deadline = date('Y-m-d');
        }

        try {
            $task = Task::find($taskId);
            $task->name = $name;
            $task->deadline = $deadline;
            $task->save();
        } catch (\Throwable $t) {
            throw new \Exception('Unable to update task.');
        }
        return \response()->json($task);
    }

    public function delete(Request $request)
    {
        try {
            $task = Task::find($request->id);
            $task->delete();
        } catch (\Throwable $t) {
            throw new \Exception('Unable to delete task.');
        }
        return \response()->json($task);
    }

    public function complete(Request $request)
    {
        $task = Task::find($request->id);
        $task->status = 1;
        $task->priority = 0;
        $task->save();

        return \response()->json($task);
    }

    public function swapPriority(Request $request)
    {
        $service = &$this->taskService;
        $isOk = $service->swapPriority(
            $request->current_task_id,
            $request->current_priority,
            $request->next_task_id,
            $request->next_priority
        );

        $response = $request->all();
        $response['isOk'] = $isOk;

        return \response()->json($response);
    }
}
