<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProjectController extends Controller
{
    public function create(Request $request)
    {
        try {
            $newProject = new Project();
            $newProject->name = $request->name;
            $newProject->user_id = Auth::user()->id;
            $newProject->save();
        } catch (\Throwable $t) {
            Log::log('error', $t->getMessage(), ['trace' => $t->getTrace()]);
            throw new \Exception('Can\'t add project.');
        }
        return response()->json($newProject);
    }

    public function edit(Request $request)
    {
        try {
            Project::whereId($request->id)
                ->update(['name' => $request->name]);
        } catch (\Throwable $t) {
            Log::log('error', $t->getMessage(), ['trace' => $t->getTrace()]);
            throw new \Exception('Unable to update project.');
        }

        return \response()->json(['id' => $request->id, 'name' => $request->name]);
    }

    public function delete(Request $request)
    {
        try {
            Project::whereId($request->id)->delete();
        } catch (\Throwable $t) {
            Log::log('error', $t->getMessage(), ['trace' => $t->getTrace()]);
            throw new \Exception('Unable to delete project.');
        }
        return \response()->json($request->id);
    }
}
