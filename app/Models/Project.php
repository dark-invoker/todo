<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';

    protected $table = 't_projects';

    public $timestamps = true;

    public const TABLE = 't_projects';

    public function scopeWithTasks($q)
    {
        return $q
            ->leftJoin(Task::TABLE, Task::TABLE . '.project_id', Project::TABLE . '.id');
    }

    public function scopeWithUsers($q)
    {
        return $q
            ->join(User::TABLE, User::TABLE . '.id', Project::TABLE . '.user_id');
    }

    public function scopeByUserId($q, int $userId)
    {
        return $q
            ->where(Project::TABLE . '.user_id', $userId);
    }

    public function scopeByProjectId($q, int $projectId)
    {
        return $q
            ->where(self::TABLE . '.id', $projectId);
    }
}
