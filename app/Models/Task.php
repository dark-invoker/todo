<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';

    protected $table = 't_tasks';

    public $timestamps = true;

    public const TABLE = 't_tasks';

}
